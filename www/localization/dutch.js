var DUTCH_TRANSLATIONS = {
    // Global
    app_name: "Veggi App",

    // Login
    login: "log in",
    register: "registreer",

    // Intro
    start_app: "start app",

    // Home
    home: "hoofdpagina",
    settings: "instellingen",
    logout: "uitloggen",

    recipe_select: {
        title: "wat eten we vanavond?",
        subtitle: "tap om het uit te vinden!"
    },

    // Habits
    you_are_a: "je bent een",
    habits: {
        info: "Geef aan hoeveel dagen van de week je vlees, vis en/of zuivel je eet."
    },

    // Articles
    daily_article: "dagelijks artikel",
    all_articles: "alle artikelen",
    read_article: "lees artikel",
    sources: "bronnen",
    related_articles: "gerelateerde artikelen",
    article: "artikel",

    statistics: {
        statistics: "statistieken",
        time_left: {
            no_progress: "geen progressie",
            prefix: "nog",
            postfix: "te gaan",
            year: "jaar",
            years: "jaar",
            month: "maand",
            months: "maanden",
            week: "week",
            weeks: "weken",
            day: "dag",
            days: "dagen",
            hour: "uur",
            hours: "uur",
            minute: "minuut",
            minutes: "minuten",
            a_few_seconds: "een paar seconden"
        }
    },

    // Recipes
    favorites: "favorieten",
    all_recipes: "alle recepten",
    random_recipe: "willekeurig recept",

    // Recipe
    recipe: "recept",
    today_i_am_eating: "vandaag eet ik",
    vegan: "vegan",
    finished_in: "klaar in",
    minutes: "minuten",
    select: "selecteer",
    deselect: "geselecteerd",
    make_favorite: "maak favoriet",
    favorite: "favoriet",
    difficulty: "moeilijkheidsgraad",
    price: "prijs",
    ingredients: "ingredienten",
    tips: "tips",
    requirements: "extra benodigdheden",
    instructions: "bereidingswijze",

    // Settings
    save: "opslaan",
    cancel: "annuleer",
    reset_statistics: {
        title: "reset statistieken",
        question: "weet je zeker dat je al je voortgang wilt verwijderen?",
        confirm: "verwijder",
        cancel: "annuleer",
        done: {
            title: "statistieken verwijderd",
            ok: "ok"
        }
    }

};