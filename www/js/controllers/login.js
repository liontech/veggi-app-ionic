app.controller('LoginCtrl', function($scope, $http, $ionicLoading, $state, $rootScope, $localStorage, $veggiData, $userData){

    var showWarning = function(message){
        $scope.warning.message = message;
        $scope.warning.must_show = true;
    };

    var hideWarning = function() {
        $scope.warning.must_show = false;
    };

    var handleLoginCallback = function(resp, isRegistered) {
        if (resp.data.success){

            $veggiData.initialize(resp.data.global_data);
            $userData.initialize(resp.data.user_data);

            var veggiData = $veggiData.get();
            var userData = $userData.get();

            userData.recipe_data.favorites.forEach(function(favoriteRecipe){
                favoriteRecipe.recipe = findByProperty(veggiData.recipes, 'id', favoriteRecipe.recipe_id);
            });

            $localStorage.setObject("login_data", $scope.login_data);

            if (isRegistered) {
                $state.go('intro');
            }else{
                $state.go('home');
            }

            hideWarning();
        }else{
            showWarning(resp.data.msg);
        }
        $ionicLoading.hide();
    };

    var performLogin = function(email, password){
        var params = {
            email: email,
            password: password
        };

        $ionicLoading.show();

        $http.post(BACKEND_URL + 'login/', params).then(function(resp){
            handleLoginCallback(resp, false);
        }, function(err){
            console.log('Error', err);
            $ionicLoading.hide();
        });
    };

    $scope.login_data = $localStorage.getObject("login_data", {
        email: "",
        password: ""
    });

    $scope.warning = {
        message: '',
        must_show: false
    };

    $scope.onLoginClicked = function(){
        performLogin($scope.login_data.email, $scope.login_data.password);
    };

    $scope.onRegisterClicked = function() {
        var params = {
            email: $scope.login_data.email,
            password: $scope.login_data.password
        };

        $ionicLoading.show();

        $http.post(BACKEND_URL + 'register/', params).then(function(resp){
            handleLoginCallback(resp, true);
        });
    };

});