app.controller('ArticlesCtrl', function($scope, $veggiData){

    var articles = $veggiData.get().articles.slice();
    articles.sort(function(a, b){return a.stage.order - b.stage.order;});
    $scope.articles = articles;

    $scope.categories = $veggiData.get().article_categories;

});