app.controller('HabitsCtrl', function($scope, $userData, $state, $http, $ionicLoading) {

    var reset = function() {
        $scope.habits = JSON.parse(JSON.stringify($userData.get().habits));
    };

    var override = function() {
        var user_data = $userData.get();
        user_data.habits = $scope.habits;

        user_data.statistics.forEach(function(statistic){
            statistic.value = statistic.getUpdatedValue();
            user_data.updateHabitsInitTime();
            statistic.updateDailyIncrease();
        });
    }

    reset();

    $scope.onCancelClicked = function() {
        reset();
        $state.go('home');
    };

    $scope.onSaveClicked = function() {

        override();

        var params = {
            'habits': $scope.habits
        };

        $ionicLoading.show();

        $http.post(BACKEND_URL + 'habits/change/', params).then(function(resp) {
            $ionicLoading.hide();
            $state.go('home');
        });
    };

});