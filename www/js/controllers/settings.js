app.controller("SettingsCtrl", function($scope, $ionicPopup, translateFilter, capitalizeFilter, $userData, $http){

    $scope.resetStatistics = function() {
        $ionicPopup.confirm({
            title: capitalizeFilter(translateFilter("reset_statistics.title")),
            template: capitalizeFilter(translateFilter("reset_statistics.question")),
            cancelText: capitalizeFilter(translateFilter("reset_statistics.cancel")),
            okText: capitalizeFilter(translateFilter("reset_statistics.confirm"))
        }).then(function(isConfirmed) {
            if(!isConfirmed) { return; }
            $userData.get().statistics.forEach(function(statistic){
                statistic.value = 0;
                statistic.units.forEach(function(unit){
                    unit.progress = 0;
                    unit.value = 0;
                });
            });
            $http.post(BACKEND_URL + 'statistics/reset/').then(function(resp) {
                $ionicPopup.alert({
                    title: capitalizeFilter(translateFilter("reset_statistics.done.title")),
                    okText: capitalizeFilter(translateFilter("reset_statistics.done.ok"))
                })
            });
        });
    };

});