app.controller("DailyArticleCtrl", function($scope, $dailyArticle, $ionicViewSwitcher, $state){

    $scope.article = $dailyArticle.get();

    $scope.readArticle = function() {
        $ionicViewSwitcher.nextDirection('forward');
        $state.go("article", {id: $scope.article.id});
    };

});