app.controller('StatisticCtrl', function($scope, $stateParams, $userData, $http, $scopeInterval){

    $scopeInterval.intervalApply($scope);

    $scope.statistic = findByProperty($userData.get().statistics, 'meta_id', $stateParams.meta_id);

    $scope.selectUnit = function(unit){
        $scope.statistic.selected_unit = unit;

        var params = {
            'statistic_meta_id': $scope.statistic.meta_id,
            'unit_id': unit.id
        };

        $http.post(BACKEND_URL + 'statistics/units/select/', params)
    };

});