app.controller('HomeCtrl', function($scope, $http, $state, $veggiData){

    $scope.recipe = $veggiData.get().recipes[0];

    $scope.onLogoutClicked = function(){
        $http.post(BACKEND_URL + 'logout/').then(function(resp){
        }, function(err){
            console.log('Error', err);
        });
        $state.go('login');
    };

});