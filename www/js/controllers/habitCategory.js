app.controller("HabitCategoryCtrl", function($scope, $veggiData, $userData){

    var habits = $scope.habits ? $scope.habits : $userData.get().habits;
    var habitCategories = $veggiData.get().habit_categories.slice();

    var habitLookupTable = [];
    habits.forEach(function(habit){
        habitLookupTable[habit.meta_id] = habit;
    });

    $scope.getHabitCategoryTitle = function(){
        habitCategories.sort(function(a,b){return a.priority - b.priority;})

        var result = habitCategories[0];
        habitCategories.forEach(function(habitCategory){
            var valid = true;
            habitCategory.components.forEach(function(component){
                var habit = habitLookupTable[component.habit_meta_id];
                if (habit.value > component.max_days){
                    valid = false;
                }
            });
            if (valid){
                result = habitCategory;
            }
        });

        return result.title;
    };

});
