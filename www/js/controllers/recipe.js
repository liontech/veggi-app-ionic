app.controller('RecipeCtrl', function($scope, $stateParams, $veggiData, $userData, $http, $ionicLoading, $translate){

    $scope.recipe = findByProperty($veggiData.get().recipes, 'id', $stateParams.id);

    $scope.selectButton = {
        class: '',
        text: ''
    };

    $scope.favoriteButton = {
        class: '',
        text: ''
    };

    $scope.personCount =  $scope.recipe.person_count;

    var userData = $userData.get();
    var selectedRecipeId = userData.recipe_data.selected_recipe_id;

    var isSelected = selectedRecipeId != -1 ? (selectedRecipeId == $scope.recipe.id) : false;

    var favorites = userData.recipe_data.favorites;
    var isFavorite = findByProperty(favorites, "recipe_id", $scope.recipe.id) != null;

    var updateSelectButton = function(value){
        isSelected = value;
        var textKey = "deselect";
        if (isSelected){
            $scope.selectButton.class = 'button-positive';
        }else{
            $scope.selectButton.class = '';
            textKey = "select";
        }
        $translate(textKey).then(function(output){
            $scope.selectButton.text = output;
        });
    };

    var updateFavoriteButton = function(value){
        isFavorite = value;
        var textKey = "favorite";
        if (isFavorite){
            $scope.favoriteButton.class = 'button-positive';
        }else{
            $scope.favoriteButton.class = '';
            textKey = "make_favorite";
        }
        $translate(textKey).then(function(output){
            $scope.favoriteButton.text = output;
        });
    };

    updateSelectButton(isSelected);
    updateFavoriteButton(isFavorite);

    var selectRecipe = function(){
        var params = {
            'recipe_id': $scope.recipe.id
        };

        userData.recipe_data.selected_recipe_id = $scope.recipe.id;

        $ionicLoading.show();

        $http.post(BACKEND_URL + 'recipes/select/', params).then(function(resp) {
            $ionicLoading.hide();
            updateSelectButton(true);
        });
    };

    var deselectRecipe = function(){
        userData.recipe_data.selected_recipe_id = -1;

        $ionicLoading.show();

        $http.post(BACKEND_URL + 'recipes/deselect/').then(function(resp) {
            $ionicLoading.hide();
            updateSelectButton(false);
        });
    };

    var favoriteRecipe = function() {
        var params = {
            'recipe_id': $scope.recipe.id
        };

        userData.recipe_data.favorites.push({
            recipe_id: $scope.recipe.id,
            recipe: $scope.recipe,
            last_selected: 0
        });

        $ionicLoading.show();

        $http.post(BACKEND_URL + 'recipes/favorite/', params).then(function(resp) {
            $ionicLoading.hide();
            updateFavoriteButton(true);
        });
    };

    var unfavoriteRecipe = function() {
        var params = {
            'recipe_id': $scope.recipe.id
        };

        var favorite = findByProperty(userData.recipe_data.favorites, "recipe_id", $scope.recipe.id);
        removeElement(userData.recipe_data.favorites, favorite);

        $ionicLoading.show();

        $http.post(BACKEND_URL + 'recipes/unfavorite/', params).then(function(resp) {
            $ionicLoading.hide();
            updateFavoriteButton(false);
        });
    };

    $scope.onSelectButtonClicked = function(){
        if (isSelected) {
            deselectRecipe();
        }else{
            selectRecipe();
        }
    };

    $scope.onFavoriteButtonClicked = function(){
        if (isFavorite) {
            unfavoriteRecipe();
        }else{
            favoriteRecipe();
        }
    };

});