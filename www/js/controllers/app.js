app.controller("AppCtrl", function($rootScope, $scope, $state, $ionicViewSwitcher, $ionicHistory, $ionicSideMenuDelegate){

    $rootScope.hideBackButton = true;
    $rootScope.hideMenuButton = true;

    $scope.goBack = function() {
        $ionicViewSwitcher.nextDirection('back');
        $ionicHistory.goBack();
    };

    $scope.openMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };

    $scope.isBackButtonHidden = function() {
        return $rootScope.hideBackButton || $ionicSideMenuDelegate.isOpenLeft();
    };

});
