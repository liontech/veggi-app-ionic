app.controller('StatisticsCtrl', function($scope, $userData, $scopeInterval) {

    $scope.statistics = $userData.get().statistics;

    $scopeInterval.intervalApply($scope);

});