app.controller("IntroCtrl", function($scope, $veggiData, $userData, $http, $state){

    $scope.pages = $veggiData.get().intro_pages;
    $scope.habits = JSON.parse(JSON.stringify($userData.get().habits));

    $scope.start = function() {
        $userData.get().habits = $scope.habits;

        var params = {
            'habits': $scope.habits
        };

        $http.post(BACKEND_URL + 'habits/change/', params);

        $state.go('home');
    };

});