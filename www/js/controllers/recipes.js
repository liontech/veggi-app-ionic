app.controller('RecipesCtrl', function($scope, $veggiData, $userData, $state, $ionicViewSwitcher){

    var userData = $userData.get();
    var veggiData = $veggiData.get();

    var selectedRecipeId = userData.recipe_data.selected_recipe_id;

    $scope.categories = veggiData.recipe_categories.slice();
    var allRecipes = veggiData.recipes;
    $scope.recipes = [];
    allRecipes.forEach(function(recipe){
        var favorite = findByProperty(userData.recipe_data.favorites, "recipe_id", recipe.id);
        var isFavorite = favorite != null;
        $scope.recipes.push({
            is_selected: recipe.id == selectedRecipeId,
            recipe: recipe,
            is_favorite: isFavorite,
            favorite: favorite
        });
    });

    $scope.categories.forEach(function(category){
        category.recipes = [];
        $scope.recipes.forEach(function(recipe_data){
            if (category.id == recipe_data.recipe.category_id){
                category.recipes.push(recipe_data);
            }
        });
    });

    $scope.selectRandom = function() {
        var randomRecipe = getRandomElement(allRecipes);
        $ionicViewSwitcher.nextDirection('forward');
        $state.go('recipe', { id: randomRecipe.id });
    };


});