app.controller("SideMenuCtrl", function($scope, $state, $ionicSideMenuDelegate, $ionicViewSwitcher, $translate, translateFilter, capitalizeFilter){

    $scope.buttons = [];

    var addButton = function(title, state){

        var button = {
            title: capitalizeFilter(translateFilter(title)),
            onClick: function() {
                $ionicViewSwitcher.nextDirection('forward');
                $ionicViewSwitcher.nextTransition('none');
                $state.go(state);
                $ionicSideMenuDelegate.toggleLeft();
            }
        };

        $scope.buttons.push(button);
    };

    addButton("home", "home");
    addButton("all_articles", "articles");
    addButton("all_recipes", "recipes");
    addButton("settings", "settings");

});