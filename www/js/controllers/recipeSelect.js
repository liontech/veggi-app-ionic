app.controller('RecipeSelectCtrl', function($scope, $userData, $state, $veggiData, $ionicViewSwitcher){

    var userData = $userData.get();
    var selectedRecipeId = userData.recipe_data.selected_recipe_id;
    if (selectedRecipeId != -1) {
        $scope.recipe = findByProperty($veggiData.get().recipes, 'id', selectedRecipeId);
    }else{
        $scope.recipe = null;
    }

    $scope.onClick = function() {
        $ionicViewSwitcher.nextDirection('forward');
        if (selectedRecipeId != -1) {
            $state.go("recipe", {id: selectedRecipeId});
        }else{
            $state.go("recipes");
        }
    };

});