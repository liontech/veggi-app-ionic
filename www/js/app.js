var app = angular.module('vegan-app', ['ionic', 'pascalprecht.translate']);

var interceptor = function($userData) {
	return {
		request: function(config){
			if (config.url.indexOf(BACKEND_URL) == -1) { return config; }
			var userData = $userData.data;
			if (userData) {
				config.headers['Authorization'] = userData ? userData.token : '';
			}
			return config;
		},
		response: function(resp){
			return resp;
		}
	}
};

app.config(function($stateProvider, $urlRouterProvider, $httpProvider, $translateProvider) {

	$httpProvider.interceptors.push(interceptor);

	$urlRouterProvider.otherwise('/');

	var addState = function(name, url, template, ctrl) {
		$stateProvider.state(name, {
			url: url,
			templateUrl: template,
			controller: ctrl,
			cache: false
		});
	};

	addState('login', '/', 'html/states/login.html', 'LoginCtrl');
	addState('intro', '/intro', 'html/states/intro.html', 'IntroCtrl');
	addState('home', '/home', 'html/states/home.html', 'HomeCtrl');
	addState('article', '/article/:id', 'html/states/article.html', 'ArticleCtrl');
	addState('articles', '/articles', 'html/states/articles.html', 'ArticlesCtrl');
	addState('recipe', '/recipe/:id', 'html/states/recipe.html', 'RecipeCtrl');
	addState('recipes', '/recipes', 'html/states/recipes.html', 'RecipesCtrl');
	addState('habits', '/habits', 'html/states/habits.html', 'HabitsCtrl');
	addState('statistic', '/statistic/:meta_id/:personal', 'html/states/statistic.html', 'StatisticCtrl');
	addState('settings', '/settings', 'html/states/settings.html', 'SettingsCtrl');

	$translateProvider.translations('dutch', DUTCH_TRANSLATIONS);

	$translateProvider.preferredLanguage('dutch');
	$translateProvider.useSanitizeValueStrategy('sanitize');

});

app.constant('$ionicLoadingConfig', {
	template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
});