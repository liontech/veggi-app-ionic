var findById = function (array, id){
    for (var i = 0; i < array.length; i++) {
        if (array[i].id == id) {
            return array[i];
        }
    }
    return null;
};

var findByProperty = function(array, propertyName, value){
    if (array.length == 0){ return null; }
    for (var i = 0; i < array.length; i++) {
        if (array[i][propertyName] == value) {
            return array[i];
        }
    }
    return null;
};

var removeElement = function(array, element){
    var index = array.indexOf(element);
    if (index > -1){
        array.splice(index, 1);
    }
}

var getRandomElement = function(array){
    if (array.length == 0){ return null; }
    return array[Math.floor(Math.random()*array.length)];
}

var splitArray = function(array, property){
    if (array.length == 0) { return null; }
    var originalArray = array.slice();
    var groups = [];
    while (originalArray.length > 0){
        var example = originalArray[0];
        var group = [example];
        originalArray.splice(0, 1);
        for (var i = originalArray.length-1; i >= 0; i--){
            if (originalArray[i][property] == example[property]){
                group.push(originalArray[i]);
                originalArray.splice(i, 1);
            }
        }
        groups.push(group);
    }
    return groups;
}

var pluralize = function(count, zero, single, plural){
    switch (count) {
        case 0: return count + " " + zero;
        case 1: return count + " " + single;
        default: return count + " " + plural;
    }
};