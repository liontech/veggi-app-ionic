app.directive("accordion", function($ionicScrollDelegate){

    return {

        restrict: 'E',
        scope: true,

        controller: function ($scope){

            $scope.isOpen = false;

            $scope.toggleAccordion = function() {
                $scope.isOpen = !$scope.isOpen;
                $ionicScrollDelegate.resize();
                window.setTimeout(function(){
                    $ionicScrollDelegate.resize();
                }, 10);
            };

        }

    };

});
