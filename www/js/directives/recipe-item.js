app.directive("recipeItem", [function(){

    return {
        restrict: "E",
        replace: true,
        templateUrl: "html/directives/recipe-item.html"
    };

}]);