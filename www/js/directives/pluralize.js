app.directive('pluralize', [function(){
    return {
        restrict: 'E',
        scope: {
            count: '=',
            none: '=',
            single: '=',
            plural: '='
        },
        template: '{{count}} {{getEnding()}}',
        link: function(scope, element, attrs){

            scope.getEnding = function() {
                switch (scope.count) {
                    case 0: return scope.none;
                    case 1: return scope.single;
                    default: return scope.plural;
                }
            };
        }
    }
}]);