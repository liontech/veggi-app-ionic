app.directive('progressBar', [function(){
    return {
        restrict: 'E',
        templateUrl: 'html/directives/progress-bar.html',
        transclude: true,
        scope: {
            percentage: '=',
            barClass: '@barClass'
        }
    };
}]);