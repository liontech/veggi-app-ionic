app.directive("page", function($rootScope) {
    return {
        restrict: "E",
        transclude: true,
        templateUrl: 'html/directives/page.html',
        link: function(scope, element, attrs){
            $rootScope.hideBackButton = attrs["hideBackButton"] && attrs["hideBackButton"] != "undefined";
            $rootScope.hideMenuButton = attrs["hideMenuButton"] && attrs["hideMenuButton"] != "undefined";
        }
    }
});