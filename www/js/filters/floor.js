app.filter('floor', [ function() {
    return function(input) {
        if (input === 0){return 0;}
        return input ? Math.floor(input) : null;
    };
}]);