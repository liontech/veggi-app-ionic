app.service('$localStorage', function() {

    var service = null;
    service = {
        cache: [],
        set: function(key, value) {
            window.localStorage[key] = value;
            service.cache[key] = value;
        },
        get: function(key, defaultValue) {
            return service.cache[key] || window.localStorage[key] || defaultValue;
        },
        setObject: function(key, value) {
            window.localStorage[key] = JSON.stringify(value);
            service.cache[key] = value;
        },
        getObject: function(key, defaultValue) {
            if (!service.cache[key]){
                if (window.localStorage[key] && window.localStorage[key] !== "undefined") {
                    service.cache[key] = JSON.parse(window.localStorage[key]);
                }else{
                    return defaultValue;
                }
            }
            return service.cache[key];
        },
        delete: function(key){
            window.localStorage.removeItem(key);
        }
    };

    return service;

});