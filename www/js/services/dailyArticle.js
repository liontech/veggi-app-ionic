app.service('$dailyArticle', function($veggiData, $userData){

    var service;

    service = {

        get: function() {
            var articles = $veggiData.get().articles.slice();
            articles.sort(function(a, b){return a.stage.order - b.stage.order;});

            if (articles.length == 0){return null;}

            var sortedArticles = [];

            var stages = splitArray(articles, "stage");
            stages.forEach(function(stageArticles){

                var categories = splitArray(stageArticles, "category");
                while (categories.length > 0){

                    for (var i = categories.length-1; i >= 0; i--){
                        sortedArticles.push(categories[i][0]);
                        categories[i].splice(0, 1);
                        if (categories[i].length == 0){
                            categories.splice(i, 1);
                        }
                    }

                }

            });

            var dateJoined = $userData.get().date_joined;
            var originTime = (new Date(dateJoined)).getTime();
            var currentTime = (new Date()).getTime();
            var dayMilliSeconds = 8.64e7;
            var deltaDays = Math.floor(Math.abs(currentTime - originTime)/dayMilliSeconds);

            return articles[deltaDays % articles.length];
        }

    };

    return service;

});