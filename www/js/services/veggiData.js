app.service('$veggiData', function($localStorage) {

    var service;

    service = {

        data: null,

        initialize: function(data) {

            $localStorage.setObject("veggi_data", data);

            // Intro pages
            data.intro_pages.sort(function(a, b){ return a.index - b.index });

            // Articles
            data.articles.forEach(function(article){
                article.category = findById(data.article_categories, article.category_id);
                article.stage = findById(data.article_stages, article.stage_id);

                // References
                article.references.forEach(function(reference){
                    reference.article = findById(data.articles, reference.article_id);
                });

            });

            // Recipes
            data.recipes.forEach(function(recipe){
                recipe.category = findById(data.recipe_categories, recipe.category_id);
                recipe.duration = findById(data.recipe_durations, recipe.duration_id);

                // Supplies
                recipe.supplies.forEach(function(supply){
                    supply.ingredient = findById(data.ingredients, supply.ingredient_id);
                    supply.getText = function(personCount){
                        var quantity = supply.quantity / recipe.person_count * personCount;
                        var measurementText = supply.ingredient.measurement.getText(quantity);
                        return measurementText + " " + supply.ingredient.title;
                    };
                });

                // Requirements
                recipe.requirements.forEach(function(requirement){
                    requirement.tool = findById(data.tools, requirement.tool_id);
                });

                recipe.getPrice = function(personCount){
                    return recipe.price / recipe.person_count * personCount;
                };
            });

            // Statistics
            data.statistics.forEach(function(statistic){
                statistic.units.sort(function(a,b){return b.factor - a.factor});

                // Components
                statistic.components.forEach(function(component){
                    component.habit = findById(data.habits, component.habit_meta_id);
                });
            });

            // Ingredients
            data.ingredients.forEach(function(ingredient){
                ingredient.measurement = findById(data.ingredient_measurements, ingredient.measurement_id);
            });

            // Measurements
            data.ingredient_measurements.forEach(function(measurement){

                // Units
                measurement.units.forEach(function(unit) {

                    unit.getText = function(quantity){
                        var value = unit.factor * quantity;
                        if (value < 1){
                            return value + " " + unit.title;
                        }
                        var roundedValue = Math.round( value * 10 ) / 10;
                        return roundedValue + " " + unit.title;
                    };

                });

                measurement.getText = function(quantity){
                    var bestUnit = null;
                    measurement.units.forEach(function(unit){
                        if (bestUnit == null){
                            bestUnit = unit;
                            return;
                        }
                        if (unit.getText(quantity).length < bestUnit.getText(quantity).length){
                            bestUnit = unit;
                        }
                    });
                    if (bestUnit == null){
                        return "No unit available";
                    }
                    return bestUnit.getText(quantity);
                };
            });

            service.data = data;
        },

        get: function() {
            if (service.data == null){
                var data = $localStorage.getObject("veggi_data");
                if (data == null){ return null; }
                service.initialize(data);
            }
            return service.data;
        }
    };

    return service;

});