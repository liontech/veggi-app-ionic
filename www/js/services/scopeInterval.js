app.service("$scopeInterval", function($interval){

    var INTERVAL_DURATION = 1000 * 10;

    return {

        intervalApply: function(scope) {
            var canceled = false;
            var cancel = function(){
                if (canceled){return;}
                canceled = true;
                $interval.cancel(refreshInterval);
            };
            scope.$on('$destroy', cancel);
            var refreshInterval = $interval(function () {
                if (!scope.$$phase) {
                    scope.$apply();
                }
            }, INTERVAL_DURATION);
        }

    };

});