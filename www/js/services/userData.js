app.service("$userData", function($veggiData, $localStorage, translateFilter){

    var service = null;

    service = {

        data: null,

        initialize: function(data){

            data.habits_init_time = (new Date()).getTime();
            data.updateHabitsInitTime = function(){
                data.habits_init_time = (new Date()).getTime();
            };

            $localStorage.setObject("user_data", data);

            // Habits
            data.habits.forEach(function(habit){
                habit.meta = findById($veggiData.get().habits, habit.meta_id);
            });

            // Statistics
            data.statistics.forEach(function(statistic){

                statistic.meta = findById($veggiData.get().statistics, statistic.meta_id);

                // Daily increase
                statistic.daily_increase = 0;
                statistic.updateDailyIncrease = function() {
                    statistic.daily_increase = 0;
                    statistic.meta.components.forEach(function(component){
                        var habit = findByProperty(data.habits, "meta_id", component.habit.id);
                        var average = habit.meta.average_value;
                        var effectiveness = Math.max(0, 1.0 - habit.value / average);
                        var increament = effectiveness * component.factor;
                        statistic.daily_increase += increament;
                    });
                };
                statistic.updateDailyIncrease();

                // Updated value
                statistic.getUpdatedValue = function(){
                    var currentTime = (new Date()).getTime();
                    var deltaSeconds = (currentTime - data.habits_init_time) / 1000;
                    var deltaDays = deltaSeconds / 60 / 60 / 24;
                    var increament = statistic.daily_increase * deltaDays;
                    var totalValue = statistic.value + increament;
                    return totalValue;
                };

                // Units
                statistic.units = statistic.meta.units;
                statistic.units.forEach(function(unit){
                    unit.value = Math.floor(unit.factor * statistic.value);
                    unit.progress = Math.floor((unit.factor * statistic.value - unit.value) * 100);

                    unit.getUpdatedValue = function() {
                        return Math.floor(unit.factor * statistic.getUpdatedValue());
                    };

                    unit.getTimeLeft = function(){
                        if (statistic.daily_increase == 0){ return translateFilter("statistics.time_left.no_progress"); }

                        var valueLeft = (unit.getUpdatedValue() + 1) - statistic.getUpdatedValue() * unit.factor;
                        var totalDaysLeft = valueLeft / unit.factor / statistic.daily_increase;

                        var getCountText = function() {

                            var getPluralizedTranslatedPostfix = function(count, single, plural){
                                var translatedSingle = translateFilter("statistics.time_left." + single);
                                var translatedPlural = translateFilter("statistics.time_left." + plural);
                                return pluralize(count, "", translatedSingle, translatedPlural);
                            };

                            var texts = [
                                { single: "year", plural: "years", factor: 1.0 / 365.0 },
                                { single: "month", plural: "months", factor: 1.0 / 30.0 },
                                { single: "week", plural: "weeks", factor: 1.0 / 7.0 },
                                { single: "day", plural: "days", factor: 1 },
                                { single: "hour", plural: "hours", factor: 24 },
                                { single: "minute", plural: "minutes", factor: 24 * 60, isLast: true }
                            ];

                            for (var i = 0; i < texts.length; i++){
                                var countLeft = Math.round(totalDaysLeft * texts[i].factor);
                                if (countLeft > 1 || (texts[i].isLast == true && countLeft >= 1)) {
                                    return getPluralizedTranslatedPostfix(countLeft, texts[i].single, texts[i].plural);
                                }
                            }

                            return translateFilter("statistics.time_left.a_few_seconds");
                        };

                        var prefix = translateFilter("statistics.time_left.prefix");
                        var postfix = translateFilter("statistics.time_left.postfix");
                        var result = prefix + " " + getCountText() + " " + postfix;
                        return result.trim();
                    };

                    unit.getUpdatedProgress = function() {
                        return Math.floor((unit.factor * statistic.getUpdatedValue() - unit.getUpdatedValue()) * 100);
                    };
                });

                statistic.selected_unit = findById(statistic.units, statistic.selected_unit_id);
                if (statistic.selected_unit == null){
                    statistic.selected_unit = statistic.units[0];
                }

            });

            service.data = data;

        },

        get: function(){
            if (service.data == null){
                var data = $localStorage.getObject("user_data");
                if (data == null){ return null; }
                service.initialize(data);
            }
            return service.data;

        }

    };

    return service;

});